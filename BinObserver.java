package observer;

public class BinObserver extends Observer {

    public BinObserver(Subject s) {
        subj = s;
        subj.attach(this);
    }

    public void update() {
        System.out.print(" " + Integer.toBinaryString(subj.getState()));
    }
}
